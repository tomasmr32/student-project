﻿namespace Student.TeacherAdd
{
    partial class TeacherAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.Dalykai = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbxdalykai = new System.Windows.Forms.ComboBox();
            this.txtPavarde = new System.Windows.Forms.TextBox();
            this.txtVardas = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vardas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pavarde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dalykas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(274, 105);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(113, 35);
            this.btnAdd.TabIndex = 21;
            this.btnAdd.Text = "Prideti";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Dalykai
            // 
            this.Dalykai.AutoSize = true;
            this.Dalykai.Location = new System.Drawing.Point(40, 116);
            this.Dalykai.Name = "Dalykai";
            this.Dalykai.Size = new System.Drawing.Size(42, 13);
            this.Dalykai.TabIndex = 20;
            this.Dalykai.Text = "Dalykai";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Pavarde";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Vardas";
            // 
            // cmbxdalykai
            // 
            this.cmbxdalykai.FormattingEnabled = true;
            this.cmbxdalykai.Location = new System.Drawing.Point(103, 108);
            this.cmbxdalykai.Name = "cmbxdalykai";
            this.cmbxdalykai.Size = new System.Drawing.Size(145, 21);
            this.cmbxdalykai.TabIndex = 15;
            this.cmbxdalykai.SelectedIndexChanged += new System.EventHandler(this.cmbxdalykai_SelectedIndexChanged);
            // 
            // txtPavarde
            // 
            this.txtPavarde.Location = new System.Drawing.Point(103, 71);
            this.txtPavarde.Name = "txtPavarde";
            this.txtPavarde.Size = new System.Drawing.Size(145, 20);
            this.txtPavarde.TabIndex = 12;
            // 
            // txtVardas
            // 
            this.txtVardas.Location = new System.Drawing.Point(103, 35);
            this.txtVardas.Name = "txtVardas";
            this.txtVardas.Size = new System.Drawing.Size(145, 20);
            this.txtVardas.TabIndex = 11;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Vardas,
            this.Pavarde,
            this.Dalykas});
            this.dataGridView1.Location = new System.Drawing.Point(36, 203);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(618, 210);
            this.dataGridView1.TabIndex = 22;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // Vardas
            // 
            this.Vardas.HeaderText = "Vardas";
            this.Vardas.Name = "Vardas";
            this.Vardas.ReadOnly = true;
            // 
            // Pavarde
            // 
            this.Pavarde.HeaderText = "Pavarde";
            this.Pavarde.Name = "Pavarde";
            this.Pavarde.ReadOnly = true;
            // 
            // Dalykas
            // 
            this.Dalykas.HeaderText = "Dalykas";
            this.Dalykas.Name = "Dalykas";
            this.Dalykas.ReadOnly = true;
            // 
            // TeacherAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CadetBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.Dalykai);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbxdalykai);
            this.Controls.Add(this.txtPavarde);
            this.Controls.Add(this.txtVardas);
            this.Name = "TeacherAdd";
            this.Text = "TeacherAdd";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label Dalykai;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbxdalykai;
        private System.Windows.Forms.TextBox txtPavarde;
        private System.Windows.Forms.TextBox txtVardas;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pavarde;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dalykas;
    }
}