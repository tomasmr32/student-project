﻿using Student.Dal.Models;
using Student.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student.TeacherAdd
{

    public partial class TeacherAdd : Form
    {
        private readonly TeacherService _teacherService;

        public TeacherAdd()
        {

            InitializeComponent();
            _teacherService = new TeacherService();
            uzpildykDalykus();
            uzpildykGrid();
        }

        private void cmbxdalykai_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        void uzpildykDalykus()
        {

            var dalykas = _teacherService.GaukCmbxDalykus();

            cmbxdalykai.DataSource = new BindingSource(dalykas, null);
            cmbxdalykai.DisplayMember = "Pavadinimas";
            cmbxdalykai.ValueMember = "Id";


        }
        private void uzpildykGrid()
        {
            dataGridView1.Rows.Clear();
            var duomenys = _teacherService.GaukMokytojus();

            foreach (DataRow mokytojas in duomenys.Rows)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Id"].Value = mokytojas["Id"];
                dataGridView1.Rows[index].Cells["Vardas"].Value = mokytojas["Vardas"];
                dataGridView1.Rows[index].Cells["Pavarde"].Value = mokytojas["Pavarde"];
                dataGridView1.Rows[index].Cells["Dalykas"].Value = mokytojas["Pavadinimas"];
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            if (CheckForm())
            {
                var mokytojas = new IssaugotiMokytoja
                {
                    Vardas = txtVardas.Text,
                    Pavarde = txtPavarde.Text,
                    DalykoId = (int)cmbxdalykai.SelectedValue
                };
                _teacherService.IssaugokTeacher(mokytojas);
                MessageBox.Show("Studentas pridetas sekmingai!");
                uzpildykGrid();
            }
        }
        private bool CheckForm()
        {
            if (string.IsNullOrEmpty(txtVardas.Text) || string.IsNullOrEmpty(txtPavarde.Text))
            {
                MessageBox.Show("Prasome ivesti duomenis!");
                return false;

            }

            return true;
        }
    }
}
