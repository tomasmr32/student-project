﻿using Student.Dal.Login;
using Student.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student.Login
{
    public partial class Login : Form
    {
        private readonly LoginService _loginService;
        public Login()
        {
            InitializeComponent();
            _loginService = new LoginService();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckForm())
            {

                Prisijungti prisijungti = new Prisijungti
                {
                    PrisijungimoVardas = txtPrisijungimoVardas.Text,
                    Slaptazodis = txtSlaptazodis.Text
                };

                prisijungti = _loginService.Prisijungti(prisijungti);

                if (prisijungti.Sesija == null)
                {
                    MessageBox.Show("Neteisingai ivesti duomenys");
                }
                else
                {
                    Sesija.KurkSesija(prisijungti.Sesija.UserId, prisijungti.Sesija.PrisijungimoVardas, prisijungti.Sesija.Vardas, prisijungti.Sesija.Pavarde, prisijungti.Sesija.GimimoData, prisijungti.Sesija.Statusas);
                    if (prisijungti.Sesija.Statusas == (Statusas)1)
                    {
                        this.Hide();
                        Students.Student forma = new Students.Student();
                        forma.Show();
                    }
                    else if(prisijungti.Sesija.Statusas == (Statusas)2)
                    {
                        this.Hide();
                        Teachers.Teachers forma = new Teachers.Teachers();
                        forma.Show();
                    }
                    else
                    {
                        this.Hide();
                        Admin.AdminForm forma = new Admin.AdminForm();
                        forma.Show();
                    }
                  
                }

            }
        }

        private bool CheckForm()
        {

            if (string.IsNullOrWhiteSpace(txtPrisijungimoVardas.Text) || string.IsNullOrWhiteSpace(txtSlaptazodis.Text))
            {
                MessageBox.Show("laukeliai negali buti tusti");
                return false;
            }
            return true;
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
