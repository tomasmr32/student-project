﻿namespace Student.Teachers
{
    partial class Teachers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtAprašymas = new System.Windows.Forms.TextBox();
            this.txtPazymys = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbxmokiniai = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mokinys";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtAprašymas
            // 
            this.txtAprašymas.Location = new System.Drawing.Point(99, 46);
            this.txtAprašymas.Multiline = true;
            this.txtAprašymas.Name = "txtAprašymas";
            this.txtAprašymas.Size = new System.Drawing.Size(233, 151);
            this.txtAprašymas.TabIndex = 3;
            // 
            // txtPazymys
            // 
            this.txtPazymys.Location = new System.Drawing.Point(63, 221);
            this.txtPazymys.Name = "txtPazymys";
            this.txtPazymys.Size = new System.Drawing.Size(45, 20);
            this.txtPazymys.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Aprašymas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 224);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Pazymys";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 261);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 32);
            this.button1.TabIndex = 8;
            this.button1.Text = "Issaugoti";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbxmokiniai
            // 
            this.cmbxmokiniai.FormattingEnabled = true;
            this.cmbxmokiniai.Location = new System.Drawing.Point(64, 9);
            this.cmbxmokiniai.Name = "cmbxmokiniai";
            this.cmbxmokiniai.Size = new System.Drawing.Size(137, 21);
            this.cmbxmokiniai.TabIndex = 9;
            this.cmbxmokiniai.SelectedIndexChanged += new System.EventHandler(this.cmbxmokiniai_SelectedIndexChanged);
            // 
            // Teachers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(364, 314);
            this.Controls.Add(this.cmbxmokiniai);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPazymys);
            this.Controls.Add(this.txtAprašymas);
            this.Controls.Add(this.label1);
            this.Name = "Teachers";
            this.Text = "Teachers";
            this.Load += new System.EventHandler(this.Teachers_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAprašymas;
        private System.Windows.Forms.TextBox txtPazymys;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbxmokiniai;
    }
}