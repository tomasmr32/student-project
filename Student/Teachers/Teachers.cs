﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Student.Dal.Models;
using Student.Services;

namespace Student.Teachers
{
    public partial class Teachers : Form
    {
        private readonly TeacherService _teacherService;
        public Teachers()
        {
            InitializeComponent();
            _teacherService = new TeacherService();
            uzpildykStudentus();
    }

        private void Teachers_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (CheckForm())
            {
                var mokytojas = new Mokytojai
                {
                    Mokytojas = Sesija.UserId,
                    Mokinys = (int)cmbxmokiniai.SelectedValue,
                    Aprasymas = txtAprašymas.Text,
                    Data = DateTime.Now,
                    Pazymys = Convert.ToInt32(txtPazymys.Text)
                };

                _teacherService.IssaugokPazymi(mokytojas);

                MessageBox.Show("Išsaugota!");

                txtAprašymas.Clear();
                txtPazymys.Clear();
            }
        }
        private bool CheckForm()
        {
            if (string.IsNullOrEmpty(txtAprašymas.Text) || string.IsNullOrEmpty(txtAprašymas.Text) || string.IsNullOrEmpty(txtPazymys.Text))
            {
                MessageBox.Show("Prasome ivesti duomenis!");
                return false;
            }
                return true;

        }

        void uzpildykStudentus()
        {
            var studentas = _teacherService.GaukCmbxStudentus();

            cmbxmokiniai.DataSource = new BindingSource(studentas, null);
            cmbxmokiniai.DisplayMember = "Pavadinimas";
            cmbxmokiniai.ValueMember = "Id";
        }


        private void cmbxmokiniai_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    }
