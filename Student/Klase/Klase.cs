﻿using Student.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student.Klase
{
    public partial class Klase : Form
    {
        private readonly KlaseService _service;
        public Klase()
        {
            InitializeComponent();
            _service = new KlaseService();
            Uzpildymas();
        }

        private void btnKlase_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(txtPavadinimas.Text))
            {

                var klase = new Student.Dal.Models.Klase
                {
                    Pavadinimas = txtPavadinimas.Text
                };
                _service.IssaugokKlase(klase);
                MessageBox.Show("Klasė sukurta!");
                Uzpildymas();

            }
            else
            {
                MessageBox.Show("Laukelis negali buti tuscias!");
            }
        }

        private void Uzpildymas()
        {
            dataGridView1.Rows.Clear();
            var duomenys = _service.GaukKlases();

            foreach (DataRow knyga in duomenys.Rows)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Id"].Value = knyga["Id"];
                dataGridView1.Rows[index].Cells["Pavadinimas"].Value = knyga["Pavadinimas"];
            }
        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Klase_Load(object sender, EventArgs e)
        {

        }
    }
}
