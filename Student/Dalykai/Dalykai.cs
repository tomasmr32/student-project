﻿using Student.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student.Dalykai
{
    public partial class Dalykai : Form
    {

        private readonly DalykaiService _dalykaiService;
        public Dalykai()
        {
            InitializeComponent();
            _dalykaiService = new DalykaiService();
            DalykuSarasas();

        }

        private void txtPastabos_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Dalykai_Load(object sender, EventArgs e)
        {

        }

        private void btnissaugok_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(txtdalykai.Text))
            {
                var dalykas = new Student.Dal.Models.Dalykai
                {
                    Pavadinimas = txtdalykai.Text
                };

                _dalykaiService.IssaugokDalyka(dalykas);
                MessageBox.Show("Dalykas issaugotas!");
                DalykuSarasas();
            }
            else
            {
                MessageBox.Show("Laukelis negali buti tuscias!");
            }
        }

        private void DalykuSarasas()
        {
            dataGridView1.Rows.Clear();
            var duomenys = _dalykaiService.Gaukdalykus();

            foreach (DataRow dalykas in duomenys.Rows)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Id"].Value = dalykas["Id"];
                dataGridView1.Rows[index].Cells["Pavadinimas"].Value = dalykas["Pavadinimas"];
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
