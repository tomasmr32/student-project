﻿using Student.Dal.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student
{
    public static class Sesija
    {
        public static int UserId { get; set; }

        public static string PrisijungimoVardas { get; set; }

        public static string Vardas { get; set; }

        public static string Pavarde { get; set; }

        public static DateTime GimimoData { get; set; }

        public static Statusas Statusas { get; set; }

        public static void KurkSesija(int userId, string prisijungimoVardas, string vardas, string pavarde, DateTime gimimoData, Statusas statusas)
        {
            UserId = userId;
            PrisijungimoVardas = prisijungimoVardas;
            Vardas = vardas;
            Pavarde = pavarde;
            GimimoData = gimimoData;
            Statusas = statusas;
        }
    }
}
