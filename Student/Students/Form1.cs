﻿using Student.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student.Students
{
    public partial class Student : Form
    {
        private readonly StudentService _studentService;
        public Student()
        {
            InitializeComponent();
            _studentService = new StudentService();
            uzpildykGrid();
        }


        private void uzpildykGrid()
        {
            dataGridView1.Rows.Clear();
            var duomenys = _studentService.GaukPazymius(Sesija.UserId);

            foreach (DataRow studentas in duomenys.Rows)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Id"].Value = studentas["Id"];
                dataGridView1.Rows[index].Cells["Mokytojas"].Value = studentas["Vardas"];
                dataGridView1.Rows[index].Cells["Dalykas"].Value = studentas["Pavadinimas"];
                dataGridView1.Rows[index].Cells["Pazymys"].Value = studentas["Pazymys"];
                dataGridView1.Rows[index].Cells["Data"].Value = studentas["Data"];
                dataGridView1.Rows[index].Cells["Aprasymas"].Value = studentas["Aprasymas"];
            }
        }

        private void Student_Load(object sender, EventArgs e)
        {

        }
    }
}
