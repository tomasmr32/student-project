﻿namespace Student.Admin
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTeacherAdd = new System.Windows.Forms.Button();
            this.BtnStudentAdd = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAddDalykai = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTeacherAdd
            // 
            this.btnTeacherAdd.Location = new System.Drawing.Point(12, 12);
            this.btnTeacherAdd.Name = "btnTeacherAdd";
            this.btnTeacherAdd.Size = new System.Drawing.Size(170, 23);
            this.btnTeacherAdd.TabIndex = 0;
            this.btnTeacherAdd.Text = "Pridėti mokytoją";
            this.btnTeacherAdd.UseVisualStyleBackColor = true;
            this.btnTeacherAdd.Click += new System.EventHandler(this.btnTeacherAdd_Click);
            // 
            // BtnStudentAdd
            // 
            this.BtnStudentAdd.Location = new System.Drawing.Point(12, 51);
            this.BtnStudentAdd.Name = "BtnStudentAdd";
            this.BtnStudentAdd.Size = new System.Drawing.Size(170, 23);
            this.BtnStudentAdd.TabIndex = 1;
            this.BtnStudentAdd.Text = "Pridėti studentą";
            this.BtnStudentAdd.UseVisualStyleBackColor = true;
            this.BtnStudentAdd.Click += new System.EventHandler(this.BtnStudentAdd_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Pridėti klases";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAddDalykai
            // 
            this.btnAddDalykai.Location = new System.Drawing.Point(12, 123);
            this.btnAddDalykai.Name = "btnAddDalykai";
            this.btnAddDalykai.Size = new System.Drawing.Size(170, 23);
            this.btnAddDalykai.TabIndex = 3;
            this.btnAddDalykai.Text = "Pridėti dalykus";
            this.btnAddDalykai.UseVisualStyleBackColor = true;
            this.btnAddDalykai.Click += new System.EventHandler(this.btnAddDalykai_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 158);
            this.Controls.Add(this.btnAddDalykai);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BtnStudentAdd);
            this.Controls.Add(this.btnTeacherAdd);
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTeacherAdd;
        private System.Windows.Forms.Button BtnStudentAdd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnAddDalykai;
    }
}