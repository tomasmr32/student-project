﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student.Admin
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();
        }

        private void btnTeacherAdd_Click(object sender, EventArgs e)
        {
            TeacherAdd.TeacherAdd forma = new TeacherAdd.TeacherAdd();
            forma.ShowDialog();
        }

        private void BtnStudentAdd_Click(object sender, EventArgs e)
        {
            StudentAdd.StudentAdd forma = new StudentAdd.StudentAdd();
            forma.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Klase.Klase forma = new Klase.Klase();
            forma.ShowDialog();
        }

        private void btnAddDalykai_Click(object sender, EventArgs e)
        {
            Dalykai.Dalykai forma = new Dalykai.Dalykai();
            forma.ShowDialog();
        }
    }
}
