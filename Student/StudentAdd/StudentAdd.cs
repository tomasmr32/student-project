﻿using Student.Dal.Models;
using Student.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Student.StudentAdd
{
    public partial class StudentAdd : Form
    {

        private readonly KlaseService _klaseService;
        private readonly StudentService _studentService;      
        public StudentAdd()
        {
            InitializeComponent();
            _klaseService = new KlaseService();
            uzpildykKlases();
            _studentService = new StudentService();
            uzpildykGrid();
        }

        void uzpildykKlases()
        {
            var klase = _klaseService.GaukCmbxKlases();

            cmbxKlase.DataSource = new BindingSource(klase, null);
            cmbxKlase.DisplayMember = "Pavadinimas";
            cmbxKlase.ValueMember = "Id";
        }


        private void Klase_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (CheckForm())
            {
                var studentas = new Studentai
                {
                    Vardas = txtVardas.Text,
                    Pavarde = txtPavarde.Text,
                    TelNr = txtTelNr.Text,
                    Adresas = txtAdresas.Text,  
                    KlasesId = (int)cmbxKlase.SelectedValue
                };
                _studentService.IssaugokStudent(studentas);
                MessageBox.Show("Studentas pridetas sekmingai!");
                uzpildykGrid();
            }
        }
        private bool CheckForm()
        {
            if (string.IsNullOrEmpty(txtVardas.Text) || string.IsNullOrEmpty(txtPavarde.Text) || string.IsNullOrEmpty(txtTelNr.Text) || string.IsNullOrEmpty(txtAdresas.Text))
            {
                MessageBox.Show("Prasome ivesti duomenis!");
                return false;

            }

            return true;
        }

        private void uzpildykGrid()
        {
            dataGridView1.Rows.Clear();
            var duomenys = _studentService.GaukStudentus();

            foreach (DataRow studentas in duomenys.Rows)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Id"].Value = studentas["Id"];
                dataGridView1.Rows[index].Cells["Vardas"].Value = studentas["Vardas"];
                dataGridView1.Rows[index].Cells["Pavarde"].Value = studentas["Pavarde"];
                dataGridView1.Rows[index].Cells["Klasė"].Value = studentas["Pavadinimas"];
            }
        }
            

        private void StudentAdd_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
