﻿namespace Student.StudentAdd
{
    partial class StudentAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtVardas = new System.Windows.Forms.TextBox();
            this.txtPavarde = new System.Windows.Forms.TextBox();
            this.cmbxKlase = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAdresas = new System.Windows.Forms.TextBox();
            this.txtTelNr = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vardas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pavarde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Klasė = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtVardas
            // 
            this.txtVardas.Location = new System.Drawing.Point(102, 18);
            this.txtVardas.Name = "txtVardas";
            this.txtVardas.Size = new System.Drawing.Size(145, 20);
            this.txtVardas.TabIndex = 0;
            // 
            // txtPavarde
            // 
            this.txtPavarde.Location = new System.Drawing.Point(102, 52);
            this.txtPavarde.Name = "txtPavarde";
            this.txtPavarde.Size = new System.Drawing.Size(145, 20);
            this.txtPavarde.TabIndex = 1;
            // 
            // cmbxKlase
            // 
            this.cmbxKlase.FormattingEnabled = true;
            this.cmbxKlase.Location = new System.Drawing.Point(102, 147);
            this.cmbxKlase.Name = "cmbxKlase";
            this.cmbxKlase.Size = new System.Drawing.Size(167, 21);
            this.cmbxKlase.TabIndex = 4;
            this.cmbxKlase.SelectedIndexChanged += new System.EventHandler(this.Klase_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Vardas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Pavarde";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Klase";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(35, 191);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(113, 35);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "Prideti";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Adresas";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "TelNr";
            // 
            // txtAdresas
            // 
            this.txtAdresas.Location = new System.Drawing.Point(102, 121);
            this.txtAdresas.Name = "txtAdresas";
            this.txtAdresas.Size = new System.Drawing.Size(145, 20);
            this.txtAdresas.TabIndex = 12;
            // 
            // txtTelNr
            // 
            this.txtTelNr.Location = new System.Drawing.Point(102, 87);
            this.txtTelNr.Name = "txtTelNr";
            this.txtTelNr.Size = new System.Drawing.Size(145, 20);
            this.txtTelNr.TabIndex = 11;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Vardas,
            this.Pavarde,
            this.Klasė});
            this.dataGridView1.Location = new System.Drawing.Point(35, 252);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(606, 223);
            this.dataGridView1.TabIndex = 15;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // Vardas
            // 
            this.Vardas.HeaderText = "Vardas";
            this.Vardas.Name = "Vardas";
            this.Vardas.ReadOnly = true;
            // 
            // Pavarde
            // 
            this.Pavarde.HeaderText = "Pavarde";
            this.Pavarde.Name = "Pavarde";
            this.Pavarde.ReadOnly = true;
            // 
            // Klasė
            // 
            this.Klasė.HeaderText = "Klasė";
            this.Klasė.Name = "Klasė";
            this.Klasė.ReadOnly = true;
            // 
            // StudentAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 487);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAdresas);
            this.Controls.Add(this.txtTelNr);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbxKlase);
            this.Controls.Add(this.txtPavarde);
            this.Controls.Add(this.txtVardas);
            this.Name = "StudentAdd";
            this.Text = "StudentAdd";
            this.Load += new System.EventHandler(this.StudentAdd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtVardas;
        private System.Windows.Forms.TextBox txtPavarde;
        private System.Windows.Forms.ComboBox cmbxKlase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAdresas;
        private System.Windows.Forms.TextBox txtTelNr;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pavarde;
        private System.Windows.Forms.DataGridViewTextBoxColumn Klasė;
    }
}