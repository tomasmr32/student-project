﻿using MySql.Data.MySqlClient;
using Student.Dal;
using Student.Dal.Login;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Services
{
    public class LoginService : Dbconnection
    {
        public LoginService()
        {

        }

        public Prisijungti Prisijungti(Prisijungti prisijungti)
        {
            var gaukNaudotojaSql = @"SELECT Id,Vardas,Pavarde,GimimoData,statusas FROM naudotojai WHERE PrisijungimoVardas=@PrisijungimoVardas AND Slaptazodis=@Slaptazodis";


            MySqlDataAdapter sda = new MySqlDataAdapter(gaukNaudotojaSql, Connection);

            sda.SelectCommand.Parameters.AddWithValue("@PrisijungimoVardas", prisijungti.PrisijungimoVardas);
            sda.SelectCommand.Parameters.AddWithValue("@Slaptazodis", prisijungti.Slaptazodis);

            DataTable dtbl = new DataTable();
            sda.Fill(dtbl);


            if (dtbl.Rows.Count == 1)
            {
                prisijungti.Sesija = new Sesija
                {
                    UserId = (int)dtbl.Rows[0]["Id"],
                    PrisijungimoVardas = prisijungti.PrisijungimoVardas,                
                    Vardas = dtbl.Rows[0]["Vardas"].ToString(),
                    Pavarde = dtbl.Rows[0]["Pavarde"].ToString(),
                    GimimoData = (DateTime)dtbl.Rows[0]["GimimoData"],
                    Statusas = (Statusas)dtbl.Rows[0]["Statusas"]
                };
            }   


            return prisijungti;
        }
    }
}
