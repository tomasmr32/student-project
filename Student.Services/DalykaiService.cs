﻿using MySql.Data.MySqlClient;
using Student.Dal;
using Student.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Services
{
    public class DalykaiService : Dbconnection
    {

        public DalykaiService()
        {

        }

        public DataTable Gaukdalykus()
        {

            var DalykuSarasasSql = @"SELECT * from dalykai";

            var DalykuSarasasCommand = new MySqlCommand(DalykuSarasasSql, Connection);

            MySqlDataAdapter DalykuSarasasAdapter = new MySqlDataAdapter(DalykuSarasasCommand);
            DataTable DalykuDataTable = new DataTable();
            DalykuSarasasAdapter.Fill(DalykuDataTable);

            return DalykuDataTable;
        }


        public void IssaugokDalyka(Dalykai dalykai)
        {
            var sukurkNaujaDalykaSql = @"INSERT INTO dalykai (Pavadinimas)
VALUES (@Pavadinimas)";

            var sukurkNaujaDalykaСommand = new MySqlCommand(sukurkNaujaDalykaSql, Connection);
            sukurkNaujaDalykaСommand.Parameters.AddWithValue("@Pavadinimas", dalykai.Pavadinimas);
       

            try
            {
                Connection.Open();
                sukurkNaujaDalykaСommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //log exeption
            }
            finally
            {
                Connection.Close();
            }
        }

    }
}
