﻿using MySql.Data.MySqlClient;
using Student.Dal;
using Student.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Services
{
    public class KlaseService : Dbconnection
    {
        public KlaseService()
        {

        }

        public void IssaugokKlase(Klase klase)       
        {
            var sukurkNaujaKlaseSql = @"INSERT INTO klases (Pavadinimas)
                                           VALUES (@Pavadinimas)";

            var sukurkNaujaKlaseCommand = new MySqlCommand(sukurkNaujaKlaseSql, Connection);

            sukurkNaujaKlaseCommand.Parameters.AddWithValue("@Pavadinimas", klase.Pavadinimas);
           

            try
            {
                Connection.Open();
                sukurkNaujaKlaseCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //log exeption
            }
            finally
            {
                Connection.Close();
            }
        }


        public DataTable GaukKlases()
        {

            var KlasiuSarasasSql = @"SELECT * from klases";

            var KlasiuSarasasCommand = new MySqlCommand(KlasiuSarasasSql, Connection);

            MySqlDataAdapter knyguSarasasAdapter = new MySqlDataAdapter(KlasiuSarasasCommand);
            DataTable KlasiuDataTable = new DataTable();
            knyguSarasasAdapter.Fill(KlasiuDataTable);

            return KlasiuDataTable;
        }

        public List<Klase> GaukCmbxKlases()
        {

            var cmdText = "SELECT * FROM klases ";

            var cmd = new MySqlCommand(cmdText, Connection);

            DataTable klases = new DataTable();


            MySqlDataAdapter klasesAdapter = new MySqlDataAdapter(cmd);

            klasesAdapter.Fill(klases);

            return klases.AsEnumerable().Select(x => new Klase
            {
                Id = x.Field<int>("Id"),
                Pavadinimas = x.Field<string>("Pavadinimas")
            }).ToList();


        }



    }
}
