﻿using MySql.Data.MySqlClient;
using Student.Dal;
using Student.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Student.Services
{
   public class TeacherService : Dbconnection
    {
        public TeacherService()
        {
           
        }

        public List<Studentas> GaukCmbxStudentus()
        {

            var cmdText = "SELECT * FROM studentai ";

            var cmd = new MySqlCommand(cmdText, Connection);

            DataTable studentas = new DataTable();


            MySqlDataAdapter studentasAdapter = new MySqlDataAdapter(cmd);

            studentasAdapter.Fill(studentas);

            return studentas.AsEnumerable().Select(x => new Studentas
            {
                Id = x.Field<int>("Id"),
                Pavadinimas = x.Field<string>("Vardas")
            }).ToList();
        }


        public List<Dalykas> GaukCmbxDalykus()
        {

            var cmdText = "SELECT * FROM dalykai ";

            var cmd = new MySqlCommand(cmdText, Connection);

            DataTable dalykas = new DataTable();


            MySqlDataAdapter dalykasAdapter = new MySqlDataAdapter(cmd);

            dalykasAdapter.Fill(dalykas);

            return dalykas.AsEnumerable().Select(x => new Dalykas
            {
                Id = x.Field<int>("Id"),
                Pavadinimas = x.Field<string>("Pavadinimas")
            }).ToList();
        }


        public void IssaugokTeacher(IssaugotiMokytoja mokytoja)
        {
            var sukurkNaujaMokytojaSql = @"INSERT INTO mokytojai (Vardas, Pavarde,DalykoId)
                                           VALUES (@Vardas, @Pavarde, @DalykoId)";

            var sukurkNaujaMokytojaCommand = new MySqlCommand(sukurkNaujaMokytojaSql, Connection);
            sukurkNaujaMokytojaCommand.Parameters.AddWithValue("@Vardas", mokytoja.Vardas);
            sukurkNaujaMokytojaCommand.Parameters.AddWithValue("@Pavarde", mokytoja.Pavarde);
            sukurkNaujaMokytojaCommand.Parameters.AddWithValue("@DalykoId", mokytoja.DalykoId);

            try
            {
                Connection.Open();
                sukurkNaujaMokytojaCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //log exeption
            }
            finally
            {
                Connection.Close();
            }
        }

        public DataTable GaukMokytojus()
        {
            var MokytojuSarasasSql = @"SELECT * FROM mokytojai left join dalykai on dalykai.Id = mokytojai.DalykoId;";

            var MokytojuSarasasCommand = new MySqlCommand(MokytojuSarasasSql, Connection);

            MySqlDataAdapter mokytojuSarasasAdapter = new MySqlDataAdapter(MokytojuSarasasCommand);
            DataTable mokytojuDataTable = new DataTable();
            mokytojuSarasasAdapter.Fill(mokytojuDataTable);

            return mokytojuDataTable;
        }

        public void IssaugokPazymi(Mokytojai mokytojas)
        {
            var IssaugokPazymiSql = @"INSERT INTO irasai (Pazymys, MokytojoId, StudentoId, DalykoId, Data, Aprasymas)
                                           VALUES (@Pazymys, @MokytojoId, @StudentoId, (SELECT DalykoId FROM mokytojai WHERE NaudotojoId = @MokytojoId), @Data, @Aprasymas)";

            var issaugokPazymiSqlCommand = new MySqlCommand(IssaugokPazymiSql, Connection);
            issaugokPazymiSqlCommand.Parameters.AddWithValue("@Pazymys", mokytojas.Pazymys);
            issaugokPazymiSqlCommand.Parameters.AddWithValue("@MokytojoId", mokytojas.Mokytojas);
            issaugokPazymiSqlCommand.Parameters.AddWithValue("@StudentoId", mokytojas.Mokinys);
            issaugokPazymiSqlCommand.Parameters.AddWithValue("@Data", mokytojas.Data);
            issaugokPazymiSqlCommand.Parameters.AddWithValue("@Aprasymas", mokytojas.Aprasymas);

            try
            {
                Connection.Open();
                issaugokPazymiSqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //log exeption
            }
            finally
            {
                Connection.Close();
            }
        }

    }
}
