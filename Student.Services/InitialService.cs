﻿using MySql.Data.MySqlClient;
using Student.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Services
{
    public static class InitialService
    {
        public static void InitialDatabase()
        {
            try
            {
                var dBConnection = new Dbconnection("Sys");
                dBConnection.Connection.Open();
                var createDbSql = "create database if not exists Student;";
                var createDbCommand = new MySqlCommand(createDbSql, dBConnection.Connection);

                var result = createDbCommand.ExecuteNonQuery();
                dBConnection.Connection.Close();
                dBConnection.ChangeConnection("StudentConnectionString");

            }
            catch (Exception ex)
            {
                
            }

        }
    }
}
