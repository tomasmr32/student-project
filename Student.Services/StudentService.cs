﻿using MySql.Data.MySqlClient;
using Student.Dal;
using Student.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Services
{
   public class StudentService : Dbconnection
    {
        public StudentService() { }

        public void IssaugokStudent(Studentai studentai)
        {
            var sukurkNaujaStudentSql = @"INSERT INTO studentai (Vardas, Pavarde,TelNr, Adresas, KlasesId)
                                           VALUES (@Vardas, @Pavarde, @TelNr, @Adresas, @KlasesId)";

            var sukurkNaujaStudentCommand = new MySqlCommand (sukurkNaujaStudentSql, Connection);
            sukurkNaujaStudentCommand.Parameters.AddWithValue("@Vardas", studentai.Vardas);
            sukurkNaujaStudentCommand.Parameters.AddWithValue("@Pavarde", studentai.Pavarde);
            sukurkNaujaStudentCommand.Parameters.AddWithValue("@Telnr", studentai.TelNr);
            sukurkNaujaStudentCommand.Parameters.AddWithValue("@Adresas", studentai.Adresas);
            sukurkNaujaStudentCommand.Parameters.AddWithValue("@KlasesId", studentai.KlasesId);

            try
            {
                Connection.Open();
                sukurkNaujaStudentCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //log exeption
            }
            finally
            {
                Connection.Close();
            }
        }

        public DataTable GaukStudentus()
        {

            var StudentuSarasasSql = @"SELECT * FROM studentai left join klases on klases.Id = studentai.KlasesId;";

            var StudentuSarasasCommand = new MySqlCommand(StudentuSarasasSql, Connection);

            MySqlDataAdapter studentuSarasasAdapter = new MySqlDataAdapter(StudentuSarasasCommand);
            DataTable StudentuDataTable = new DataTable();
            studentuSarasasAdapter.Fill(StudentuDataTable);

            return StudentuDataTable;
        }

        public DataTable GaukPazymius(int studentId)
        {

            var StudentuPazymiaiSql = @"SELECT irasai.*, mokytojai.Vardas, dalykai.Pavadinimas from irasai left join mokytojai on mokytojai.Id = MokytojoId left join dalykai on dalykai.Id = irasai.DalykoId where irasai.StudentoId=@Id;";

            var StudentuPzymiaiCommand = new MySqlCommand(StudentuPazymiaiSql, Connection);

            StudentuPzymiaiCommand.Parameters.AddWithValue("@Id", studentId);

            MySqlDataAdapter studentuPazymiaiAdapter = new MySqlDataAdapter(StudentuPzymiaiCommand);
            DataTable StudentuDataTable = new DataTable();
            studentuPazymiaiAdapter.Fill(StudentuDataTable);

            return StudentuDataTable;
        }

    }
}
