CREATE DATABASE  IF NOT EXISTS `student` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `student`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: student
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dalykai`
--

DROP TABLE IF EXISTS `dalykai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dalykai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pavadinimas` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dalykai`
--

LOCK TABLES `dalykai` WRITE;
/*!40000 ALTER TABLE `dalykai` DISABLE KEYS */;
INSERT INTO `dalykai` VALUES (1,'Geografija'),(2,'Matematika');
/*!40000 ALTER TABLE `dalykai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `irasai`
--

DROP TABLE IF EXISTS `irasai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `irasai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pazymys` int(11) DEFAULT NULL,
  `MokytojoId` int(11) DEFAULT NULL,
  `StudentoId` int(11) DEFAULT NULL,
  `DalykoId` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Aprasymas` varchar(200) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `MokytojoId` (`MokytojoId`),
  KEY `StudentoId` (`StudentoId`),
  CONSTRAINT `irasai_ibfk_1` FOREIGN KEY (`MokytojoId`) REFERENCES `mokytojai` (`Id`),
  CONSTRAINT `irasai_ibfk_2` FOREIGN KEY (`StudentoId`) REFERENCES `studentai` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `irasai`
--

LOCK TABLES `irasai` WRITE;
/*!40000 ALTER TABLE `irasai` DISABLE KEYS */;
INSERT INTO `irasai` VALUES (1,8,2,3,2,'2019-05-22 17:49:30','kontrolinis tema lygtys'),(2,6,2,5,2,'2019-05-22 17:53:42','Kontrolinis darbas tema funkcijos');
/*!40000 ALTER TABLE `irasai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klases`
--

DROP TABLE IF EXISTS `klases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `klases` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pavadinimas` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klases`
--

LOCK TABLES `klases` WRITE;
/*!40000 ALTER TABLE `klases` DISABLE KEYS */;
INSERT INTO `klases` VALUES (1,'3g'),(2,'2h'),(3,'1a'),(4,'4b');
/*!40000 ALTER TABLE `klases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mokytojai`
--

DROP TABLE IF EXISTS `mokytojai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mokytojai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Vardas` varchar(250) DEFAULT NULL,
  `Pavarde` varchar(250) DEFAULT NULL,
  `DalykoId` int(11) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT '0',
  `NaudotojoId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `DalykoId` (`DalykoId`),
  KEY `NaudotojoId` (`NaudotojoId`),
  CONSTRAINT `mokytojai_ibfk_1` FOREIGN KEY (`DalykoId`) REFERENCES `dalykai` (`Id`),
  CONSTRAINT `mokytojai_ibfk_2` FOREIGN KEY (`NaudotojoId`) REFERENCES `naudotojai` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mokytojai`
--

LOCK TABLES `mokytojai` WRITE;
/*!40000 ALTER TABLE `mokytojai` DISABLE KEYS */;
INSERT INTO `mokytojai` VALUES (1,'Aušra','Augustovska',2,0,2),(2,'Jolanta','Kniškienė',1,0,9);
/*!40000 ALTER TABLE `mokytojai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mokytojuklases`
--

DROP TABLE IF EXISTS `mokytojuklases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mokytojuklases` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `KlasesId` int(11) DEFAULT NULL,
  `MokytojoId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `KlasesId` (`KlasesId`),
  KEY `MokytojoId` (`MokytojoId`),
  CONSTRAINT `mokytojuklases_ibfk_1` FOREIGN KEY (`KlasesId`) REFERENCES `klases` (`Id`),
  CONSTRAINT `mokytojuklases_ibfk_2` FOREIGN KEY (`MokytojoId`) REFERENCES `mokytojai` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mokytojuklases`
--

LOCK TABLES `mokytojuklases` WRITE;
/*!40000 ALTER TABLE `mokytojuklases` DISABLE KEYS */;
/*!40000 ALTER TABLE `mokytojuklases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `naudotojai`
--

DROP TABLE IF EXISTS `naudotojai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `naudotojai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PrisijungimoVardas` varchar(100) DEFAULT NULL,
  `Slaptazodis` varchar(100) DEFAULT NULL,
  `Vardas` varchar(100) DEFAULT NULL,
  `Pavarde` varchar(100) DEFAULT NULL,
  `GimimoData` date DEFAULT NULL,
  `statusas` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `statusas` (`statusas`),
  CONSTRAINT `naudotojai_ibfk_1` FOREIGN KEY (`statusas`) REFERENCES `statusas` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `naudotojai`
--

LOCK TABLES `naudotojai` WRITE;
/*!40000 ALTER TABLE `naudotojai` DISABLE KEYS */;
INSERT INTO `naudotojai` VALUES (1,'Mantas2019','123','Mantas','Butkus','2010-04-12',1),(2,'Lukas20','1231','Lukas','Stonkus','2012-08-14',2),(3,'Administratorius','admin','Matas','Vasiljevas','1991-02-18',3),(4,'Saulius20','1234','Saulius','pav1','2003-02-21',1),(5,'Markas20','1234','Markas','pav2','2002-02-21',1),(6,'Aurimas20','1234','Aurimas','pav3','2000-02-21',1),(7,'Gustas20','1234','Gustas','pav4','2004-02-21',1),(8,'Jonas20','1234','Jonas','pav5','2006-02-21',1),(9,'Martynas50','12345','Martynas','pav6','1995-02-21',2);
/*!40000 ALTER TABLE `naudotojai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statusas`
--

DROP TABLE IF EXISTS `statusas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `statusas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pavadinimas` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statusas`
--

LOCK TABLES `statusas` WRITE;
/*!40000 ALTER TABLE `statusas` DISABLE KEYS */;
INSERT INTO `statusas` VALUES (1,'Mokinys'),(2,'Mokytojas'),(3,'Administratorius');
/*!40000 ALTER TABLE `statusas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentai`
--

DROP TABLE IF EXISTS `studentai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `studentai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Vardas` varchar(100) DEFAULT NULL,
  `Pavarde` varchar(200) DEFAULT NULL,
  `TelNr` varchar(20) DEFAULT NULL,
  `Adresas` varchar(200) DEFAULT NULL,
  `KlasesId` int(11) NOT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `NaudotojoId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `KlasesId` (`KlasesId`),
  KEY `NaudotojoId` (`NaudotojoId`),
  CONSTRAINT `studentai_ibfk_1` FOREIGN KEY (`KlasesId`) REFERENCES `klases` (`Id`),
  CONSTRAINT `studentai_ibfk_2` FOREIGN KEY (`NaudotojoId`) REFERENCES `naudotojai` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentai`
--

LOCK TABLES `studentai` WRITE;
/*!40000 ALTER TABLE `studentai` DISABLE KEYS */;
INSERT INTO `studentai` VALUES (1,'Lukas','Pipirutis','861570765','Pilaite',1,0,1),(2,'Rokas','Bevardis','6868686868','Zemynos',1,0,4),(3,'Jolanta','Kokakola','768565847','Pilies',2,0,5),(4,'lukas','lukas','64684684','pilaite',1,0,6),(5,'Gustas','Stonkus','8646133131','Vilniaus',2,0,7),(6,'Vytenis','Balsys','86564213122','Sodo',3,0,8);
/*!40000 ALTER TABLE `studentai` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-22 18:42:46
