﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Dal.Models
{
    public class Mokytojai
    {
        public int Mokinys { get; set; }
        public int Mokytojas { get; set; }
        public DateTime Data { get; set; }
        public string Aprasymas { get; set; }
        public int Pazymys { get; set; }
    }
}
