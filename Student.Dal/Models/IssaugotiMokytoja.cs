﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Dal.Models
{
    public class IssaugotiMokytoja
    {
        public string Vardas { get; set; }
        public string Pavarde { get; set; }
        public int DalykoId { get; set; }
    }
}
