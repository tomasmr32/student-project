﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Dal
{
   public class Dbconnection
    {
        public MySqlConnection Connection { get; set; }

        public Dbconnection(string connectionName = "StudentConnectionString")
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            Connection = new MySqlConnection(connectionString);
        }

        public void ChangeConnection(string newConnectionName)
        {
            Connection.ConnectionString = ConfigurationManager.ConnectionStrings[newConnectionName].ConnectionString;
        }


    }
}
