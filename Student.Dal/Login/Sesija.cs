﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Dal.Login
{
    public class Sesija
    {
        public int UserId { get; set; }

        public string PrisijungimoVardas { get; set; }

        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public DateTime GimimoData { get; set; }

        public Statusas Statusas { get; set; }

    }
}
