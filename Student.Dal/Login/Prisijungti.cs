﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Dal.Login
{
    public class Prisijungti
    {
        public string PrisijungimoVardas { get; set; }

        public string Slaptazodis { get; set; }

        public Sesija Sesija { get; set; }
    }
}
